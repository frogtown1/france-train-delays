# High Speed Train Delays in France
This project utilizes train network data from the ['Société Nationale des Chemins de fer Français'](https://ressources.data.sncf.com/explore/?sort=modified) to observe quantitative patterns in the arrivals and departures of France's intercity high-speed rail service, TGV. To ensure general digestibility of the technical findings, this memo will emphasize on the generated visualizations.

## Build Up
The sheer density of France's railway network can make processing its data an expensive undertaking if done in whole. To avoid such an overhead while still getting adequate mileage, the scope of this research will only consider regions with the highest concentration of passenger stations. This is found using mapping resources provided by [Sébastian Rochette’s Github](https://github.com/statnmap/blog_tips/raw/master/2018-07-14-introduction-to-mapping-with-sf-and-co/data):

![](images/Map-FranceTrainStationsAll.png
)

From the image, it appears that the region with the most stations is Île-de-France, which cradles the nation's famed capital, Paris. A heatmap of the region's stations provides further insight on the extents to which passenger activity is distributed:

![](images/Map-TrainStationsAroundParis.png
)

## Train Activity: Departures and Arrivals in General
Now that the main working data has been tailored to fit the study's scope, train arrivals and departures can be studied. First, a general look at how punctual trains have been in meeting their departure schedules:

![](images/HeatMap-HSTPunctuality.png
)

Trains having largely kept consistent with meeting their departure schedules except for 2018, which can likely be explained by the [nation-wide strikes held by the public sector](https://www.theguardian.com/world/2018/mar/22/thousands-of-public-sector-workers-go-on-strike-across-france). The anomaly in April 2020 can possibly be the result of the peak of the COVID-19 lockdown hysteria.

Now to look at how regular trains have been meeting their arrival schedules:

![](images/HeatMap-HSTRegularity.png
)

## Train Activity: Departures and Arrivals by station line.
This section aims to dissect which station lines are responsible for the variances in the general arrivals and departures data. Forunately the SNCF database curates train data by line. Averages of staion line departures are calculated and visualized for a general understanding:

![](images/BarChar-HSTMeanPunctuality.png
)

Both the Europe and OUIGO lines suffer the most in punctuality performance, which makes sense for those familiar with France's railway services. The Europe line suffers from the congestion coming and going from neighboring European countries, while the OUIGO line is generally known as a low-cost alternative to the comapratively upscale TGV lines, so it isn't surprising for the lower performance to reflect accordingly. An in-depth look at their timeseries should similar stories:

![](images/multiHeatMap-HSTPunctualityTGV.png
)

![](images/multiHeatMap-HSTRegularity.png
)
